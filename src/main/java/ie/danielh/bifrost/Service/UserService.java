package ie.danielh.bifrost.Service;

import ie.danielh.bifrost.data.User;
import ie.danielh.bifrost.data.repositories.RoleRepository;
import ie.danielh.bifrost.data.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class UserService{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleRepository.findAll()));
    }

    public User findByUserName(String username) {
        return userRepository.findByUserName(username);
    }
}
