package ie.danielh.bifrost.data;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
public class Account {
    @Id
    @GeneratedValue
    private Long Id;
    private String carrierName;
    private String userName;
    private String password;
}
