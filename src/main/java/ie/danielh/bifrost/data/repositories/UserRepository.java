package ie.danielh.bifrost.data.repositories;

import ie.danielh.bifrost.data.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long>{
    User findByUserName(String username);
}
