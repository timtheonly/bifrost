package ie.danielh.bifrost.data.repositories;

import ie.danielh.bifrost.data.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository  extends JpaRepository<Role, Long>{
}
