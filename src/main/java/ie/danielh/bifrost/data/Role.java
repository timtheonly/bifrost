package ie.danielh.bifrost.data;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@Table(name="role")
public class Role {
    @Id
    @GeneratedValue
    private Long id;
    private String Name;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;
}
